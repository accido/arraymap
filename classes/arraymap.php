<?php
/**
 * Class: AMIterator
 *
 * @package ArrayMap
 * @subpackage Iterator
 *
 * @see Iterator
 * @see ArrayAccess
 * @see Countable
 * 
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 */
class AMIterator implements Iterator , ArrayAccess, Countable {

  /**
   * depth 
   * 
   * @var int
   * @access protected
   */
  protected $depth = 0;

  /**
   * position 
   * 
   * @var int
   * @access protected
   */
  protected $position = 0;

  /**
   * offset 
   * 
   * @var int
   * @access protected
   */
  protected $offset   = 0;

  /**
   * array 
   * 
   * @var FArray
   * @access protected
   */
  protected $array = null;

  /**
   * mod 
   * 
   * @var mixed
   * @access protected
   */
  protected $mod   = ArrayMap::MOD_FIXED_ARRAY;

  // private get(key,id=0) {{{ 
  /**
   * get
   * 
   * @param int $key 
   * @param int $position 
   * @access private
   * @return void
   */
  private function get($key, $position = 0){
    if (ArrayMap::MOD_BINARY_MEMORY === $this->mod)
      return $this->array[$key][$position];
    else 
      return $this->array[$position][$key];
  }
  // }}}

  // private set(key,id=0,value) {{{ 
  /**
   * set
   * 
   * @param mixed $key 
   * @param int $position 
   * @param mixed $value 
   * @access private
   * @return void
   */
  private function set($key, $position = 0, $value){
    if (ArrayMap::MOD_BINARY_MEMORY === $this->mod)
      $this->array[$key][$position] = $value;
    else
      $this->array[$position][$key] = $value;
  }
  // }}}

  // protected init(FArrayarray,depth) {{{ 
  /**
   * init
   * 
   * @param FArray $array 
   * @param int $depth 
   * @access protected
   * @return void
   */
  protected function init($array){
    if ($array instanceof ArrayMemory){
      $this->mod      = ArrayMap::MOD_BINARY_MEMORY;
      $this->depth    = $array->getDepth();
    }
    else{ 
      $this->mod      = ArrayMap::MOD_FIXED_ARRAY;
      $this->depth    = $array->getSize();
    }
    $this->array      = $array;
    $this->position = 0;
    $this->offset   = 0;
  }
  // }}}

  // public __construct(FArray array,depth) {{{ 
  /**
   * __construct
   * 
   * @param FArray $array 
   * @param int $depth 
   * @access public
   * @return void
   */
  public function __construct($array){
    $this->init($array);
  }
  // }}}

  // public setPosition(position) {{{ 
  /**
   * setPosition
   * 
   * @param int $position 
   * @access public
   * @return void
   */
  public function setPosition($position){
    $this->offset   = 0;
    $this->position = $position;
  }
  // }}}
  
  // public current() {{{ 
  /**
   * current
   * 
   * @access public
   * @return mixed
   */
  public function current(){
    return $this->get($this->position, $this->offset+1);
  }
  // }}}

  // public key() {{{ 
  /**
   * key
   * 
   * @access public
   * @return scalar
   */
  public function key (){
    return $this->offset;
  }
  // }}}

  // public next() {{{ 
  /**
   * next
   * 
   * @access public
   * @return void
   */
  public function next (){
    ++$this->offset;
  }
  // }}}

  // public rewind() {{{ 
  /**
   * rewind
   * 
   * @access public
   * @return void
   */
  public function rewind (){
    $this->offset = 0;
  }
  // }}}

  // public valid() {{{ 
  /**
   * valid
   * 
   * @access public
   * @return bool
   */
  public function valid (){
    return (($this->offset+1) < $this->depth);
  }
  // }}}

  // public offsetExists(offset) {{{ 
  /**
   * offsetExists
   * 
   * @param mixed $offset 
   * @access public
   * @return bool
   */
  public function offsetExists ($offset){
    return (($offset+1) < $this->depth);
  }
  // }}}

  // public offsetGet(offset) {{{ 
  /**
   * offsetGet
   * 
   * @param mixed $offset 
   * @access public
   * @throw Exception
   * @return mixed
   */
  public function offsetGet ($offset){
    $offset += 1;
    if ($offset >= $this->depth) return null;
    return $this->get($this->position, $offset);
  }
  // }}}

  // public offsetSet(offset,value) {{{ 
  /**
   * offsetSet
   * 
   * @param mixed $offset 
   * @param mixed $value 
   * @access public
   * @throw Exception
   * @return void
   */
  public function offsetSet ( $offset , $value ){
    $offset += 1;
    if ($offset >= $this->depth) return null;
    $this->set($this->position, $offset, $value);
  }
  // }}}

  // public offsetUnset(offset) {{{ 
  /**
   * offsetUnset
   * 
   * @param mixed $offset 
   * @access public
   * @return void
   */
  public function offsetUnset ( $offset ){
    $offset += 1;
    if ($offset >= $this->depth) return null;
    $this->set($this->position, $offset, null);
  }
  // }}}

  // public count() {{{ 
  /**
   * count
   * 
   * @access public
   * @return int
   */
  public function count (){
    return $this->depth - 1;
  }
  // }}}
}

/**
 * Class: ArrayMap
 *
 * @package ArrayMap
 *
 * @see Iterator
 * @see ArrayAccess
 * @see Countable
 * @see Serializable
 * 
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 */
class ArrayMap implements Iterator , ArrayAccess, Countable, Serializable {

  /**
   * MIN_ARRAY_LENGTH 
   * 
   * @const int
   */
  const MIN_ARRAY_LENGTH  = 0;

  /**
   * MOD_FIXED_ARRAY 
   * 
   * @const int
   */
  const MOD_FIXED_ARRAY   = 1;

  /**
   * MOD_BINARY_MEMORY 
   * 
   * @const int
   */
  const MOD_BINARY_MEMORY = 2;

  /**
   * args 
   * 
   * @var array
   * @access protected
   */
  protected $args   = array();

  /**
   * array 
   * 
   * @var mixed
   * @access protected
   */
  protected $array = null;

  /**
   * depth 
   * 
   * @var int
   * @access protected
   */
  protected $depth = 1;

  /**
   * current 
   * 
   * @var int
   * @access protected
   */
  protected $position = 0;

  /**
   * size 
   * 
   * @var int
   * @access protected
   */
  protected $size    = 0;

  /**
   * iterator 
   * 
   * @var AMIterator
   * @access protected
   */
  protected $iterator = null;

  /**
   * _isSort 
   * 
   * @var mixed
   * @access protected
   */
  protected $_isSort  = false;

  /**
   * pointer 
   * 
   * @var int
   * @access protected
   */
  protected $pointer  = 0;

  /**
   * _p 
   * 
   * @var int
   * @access protected
   */
  protected $_p = 0;

  /**
   * mod 
   * 
   * @var mixed
   * @access protected
   */
  protected $mod = self::MOD_FIXED_ARRAY;

  // protected get(key,id=0) {{{ 
  /**
   * get
   * 
   * @param int $key 
   * @param int $index
   * @access protected
   * @return void
   */
  protected function get($key, $index = 0){
    if (self::MOD_FIXED_ARRAY !== $this->mod)
      return $this->array[$key][$index];
    else 
      return $this->array[$index][$key];
  }
  // }}}

  // protected set(key,id=0,value) {{{ 
  /**
   * set
   * 
   * @param mixed $key 
   * @param int $index 
   * @param mixed $value 
   * @access protected
   * @return void
   */
  protected function set($key, $index = 0, $value){
    if (self::MOD_FIXED_ARRAY !== $this->mod)
      $this->array[$key][$index] = $value;
    else
      $this->array[$index][$key] = $value;
  }
  // }}}

  // protected _compare(a,b) {{{ 
  /**
   * _compare
   * 
   * @param mixed $left 
   * @param mixed $right
   * @access protected
   * @return void
   */
  protected function _compare(& $left, & $right){
    if (null === $left) return 1;
    if (null === $right) return -1;
    if (!($left instanceof Comparable)){
      if ($left === $right) return 0;
      return ( $left > $right ? 1 : -1 );
    }
    else{
      return $left->compareTo($right);
    }
  }
  // }}}

  
  // protected get_norm_size(size) {{{ 
  /**
   * get_norm_size
   * 
   * @param int $size 
   * @access protected
   * @return int 2^n-1
   */
  protected function get_norm_size( $size = 0 ){
    if (!$size) return self::MIN_ARRAY_LENGTH;
    $length = intval(pow(2,ceil(log($size,2))));
    return ($length >= self::MIN_ARRAY_LENGTH ? $length : self::MIN_ARRAY_LENGTH);
  }
  // }}}

// protected search(id) {{{ 
  /**
   * search
   * 
   * @param mixed $find 
   * @access protected
   * @return false|int
   */
  protected function search($find){
    if ( !$this->_isSort ) 
      return $this->simpleSearch($find);
    else 
      return $this->quickSearch($find);
  }
  // }}}

  // protected simpleSearch($find) {{{ 
  /**
   * simpleSearch
   * 
   * @param mixed $find 
   * @access protected
   * @return false|int
   */
  protected function simpleSearch($find){
    for($index = 0; $index < $this->pointer; $index++){
      $current_index  = $this->get($index);
      if ( 0 === $this->_compare($current_index, $find) ) return $index;
    }
    return false;
  }
  // }}}

  // protected quickSearch(find) {{{ 
  /**
   * quickSearch
   * 
   * @param mixed $find 
   * @access protected
   * @return false|int
   */
  protected function quickSearch( $key, $left = 0, $right = 0, $init = true ){
    
    if ( $init ){
      return $this->quickSearch( $key, 0, $this->pointer - 1, false );

    }
    else{
      if ( $left > $right ){
        return false;
      }
      $index          = intval( ( $right + $left ) / 2 );
      $current_value  = $this->get($index);
      $compare        = $this->_compare($current_value, $key);
      if (0 === $compare){
        return $index;
      }
      elseif(1 === $compare){
        return $this->quickSearch( $key, $left, $index - 1, false );
      }
      else{
        return $this->quickSearch( $key, $index + 1, $right, false );
      }
    }
  }
  // }}}

  // protected quick_sort(low,high) {{{ 
  /**
   * quick_sort
   * 
   * @param mixed $low 
   * @param mixed $high 
   * @access protected
   * @return void
   */
  protected function quick_sort( $low, $high ){
    $itemp = $low;                
    $jtemp = $high;
    $xtemp = $this->get(intval( ( $low + $high ) / 2 ));
    do {
      while($itemp < $this->pointer){
        $current_value  = $this->get($itemp);
        if(-1 !== $this->_compare($current_value, $xtemp)) break;
        ++$itemp;
      }
      while( 0 <= $jtemp){
        $current_value  = $this->get($jtemp);
        if(1 !== $this->_compare($current_value, $xtemp)) break;
        if (null === $this->get($jtemp)) $this->_p = $jtemp;
        --$jtemp;
      }
      if($itemp <= $jtemp){           
        if ($itemp < $jtemp){
          // обмен элементов местами:
          if (self::MOD_BINARY_MEMORY === $this->mod){
            $this->array->swap($itemp, $jtemp);
          }
          else{
            for($dtemp = 0; $dtemp < $this->depth; $dtemp++){
              $temp = $this->get($itemp, $dtemp);
              $this->set($itemp, $dtemp, $this->get($jtemp, $dtemp));
              $this->set($jtemp, $dtemp, $temp);
            }
          }
        }
        // переход к следующим элементам:
        $itemp++; $jtemp--;
      }
    } while($itemp <= $jtemp);
    if( $low < $jtemp ) $this->quick_sort( $low, $jtemp );
    if( $itemp < $high ) $this->quick_sort( $itemp, $high );
  }
  // }}}

  // protected do_attach(id,key,value) {{{ 
  /**
   * do_attach
   * 
   * @param int $index_cell 
   * @param mixed $key 
   * @param mixed $value 
   * @access protected
   * @return void
   */
  protected function do_attach( $index_cell, $key, $value ){
    if (self::MOD_FIXED_ARRAY !== $this->mod){
      $value = (array)$value;
      array_unshift($value, $key);
      $this->array[$index_cell] = $value;
    }
    else{
    $this->set($index_cell, 0, $key);
      if (  !is_scalar($value) &&
            !is_resource($value) &&
            !is_null($value) &&
            !($value instanceof Binary)){
        $value = (array)$value;
        if ( ($count = count($value)) >= $this->depth )
            $this->setDepth($count + 1);
        $index = 1;
        reset($value);
        do{
          $this->set($index_cell,$index++, current($value));
        }while(false !== next($value));
      }
      else{
        $this->set($index_cell, 1, $value);
      }
    }
  }
  // }}}

  /**
   * get_header
   * @return string
   */
  protected function get_header(){
    $data         = array(
      'pointer'   => $this->pointer,
      '_isSort'   => $this->_isSort,
      'size'      => $this->size,
      'args'      => $this->args,
    );
    $data         = serialize($data);
    return pack('Va*', strlen($data), $data);
  }

  /**
   * set_header
   *
   * @param string $ser
   */
  protected function set_header( &$ser ){
    $data = unserialize($ser);
    call_user_func_array( array($this,'init'), $data['args']);
    $this->pointer = $data['pointer'];
    $this->_isSort = $data['_isSort'];
    $this->setSize($data['size']);
  }

  /**
   * init_array
   *
   */
  protected function init_array($start, $end){

  }

  // public __construct(size=0,depth=1) {{{ 
  /**
   * __construct
   * 
   * @param int $size 
   * @param int $depth 
   * @access public
   * @return void
   */
  public function __construct(){
    call_user_func_array( array($this, 'init'), func_get_args());
  }
  // }}}

  // public init(size,depth) {{{ 
  /**
   * init
   * 
   * @param mixed $size 
   * @param mixed $depth 
   * @access public
   * @return void
   * @throw Exception
   */
  public function init( $size = 0, $depth = 2 ){
    $args               = func_get_args();
    $this->args         = $args;
    if ( !is_int($size) ){
      $class            = new ReflectionClass('ArrayMemory');
      $this->array      = $class->newInstanceArgs($args);
      $this->mod        = self::MOD_BINARY_MEMORY;
      $depth            = $this->array->getDepth();
      $size             = $this->array->getSize();
    }
    else{
      $this->mod        = self::MOD_FIXED_ARRAY;
    }
    if ( $depth < 2 ) throw new Exception( 'Why, do it stupid?' );
    $this->size     = $this->get_norm_size($size);
    $this->depth    = $depth;
    $this->position = 0;
    $this->_isSort  = false;
    $this->pointer  = 0;
    if (self::MOD_BINARY_MEMORY === $this->mod){
      $this->array->setSize($this->size);
    }
    else{
      $this->array = new SplFixedArray($depth);
      for($index = 0; $index < $depth; $index++){
        $this->array[$index] = new SplFixedArray($this->size);
      }
    }
    $this->iterator = new AMIterator($this->array);
  }
  // }}}

  // public sort() {{{ 
  /**
   * sort
   * 
   * @access public
   * @return void
   */
  public function sort(){
    $this->_p = $this->pointer;
    $return = $this->quick_sort( 0, $this->pointer - 1 );
    $this->pointer = $this->_p;
    $this->_isSort = true;
    return $return;
  }
  // }}}

  // public getSize() {{{ 
  /**
   * getSize
   * 
   * @access public
   * @return void
   */
  public function getSize(){
    return $this->size;
  }
  // }}}

  // public setSize(size) {{{ 
  /**
   * setSize
   * 
   * @param int $size 
   * @access public
   * @return void
   */
  public function setSize( $size ){
    if ( self::MOD_BINARY_MEMORY === $this->mod ){
      $this->array->setSize($size);
    }
    else{
      for( $index = 0; $index < $this->depth; $index++){
        $iterator = $this->array[$index];
        $iterator->setSize( $size );
      }
    }
    $this->size = $size;
  }
  // }}}

  // public getDepth() {{{ 
  /**
   * getDepth
   * 
   * @access public
   * @return void
   */
  public function getDepth(){
    return $this->depth;
  }
  // }}}

  // public setDepth(depth) {{{ 
  /**
   * setDepth
   * 
   * @param mixed $depth 
   * @access public
   * @return void
   */
  public function setDepth( $depth ){
    if (self::MOD_BINARY_MEMORY === $this->mod){
      throw new BinaryException('Column outside the range.');
    }
    if ( $depth < 2 ) throw new Exception( 'Why, do it stupid?' ); 
    $this->array->setSize( $depth );
    for( $index = $this->depth; $index < $depth; $index++){
      $this->array[$index] = new SplFixedArray($this->size);
    }
    $this->iterator = new AMIterator($this->array);
  }
  // }}}

  // public set_compare_closure(Closureclosure) {{{ 
  /**
   * set_compare_closure
   * 
   * @param Closure $closure 
   * @access public
   * @return void
   * @deprecated
   */
  public function set_compare_closure( Closure $closure ){
    $this->compare = $closure;
  }
  // }}}

  // public setSort(flag=true) {{{ 
  /**
   * setSort
   * 
   * @param bool $flag 
   * @access public
   * @return void
   */
  public function setSort( $flag = true ){
    $this->_isSort = $flag;
  }
  // }}}

  // public current() {{{ 
  /**
   * current
   * 
   * @access public
   * @return mixed
   */
  public function current (){
    if ( $this->depth > 2){
      $this->iterator->setPosition($this->position);
      $return = $this->iterator;
    }
    else{
      $return = $this->get($this->position, 1);
    }
    return $return;
  }
  // }}}

  // public key() {{{ 
  /**
   * key
   * 
   * @access public
   * @return scalar
   */
  public function key (){
    return $this->get($this->position);
  }
  // }}}

  // public next() {{{ 
  /**
   * next
   * 
   * @access public
   * @return void
   */
  public function next (){
    ++$this->position;
  }
  // }}}

  // public rewind() {{{ 
  /**
   * rewind
   * 
   * @access public
   * @return void
   */
  public function rewind (){
    $this->position = 0;
  }
  // }}}

  // public valid() {{{ 
  /**
   * valid
   * 
   * @access public
   * @return bool
   */
  public function valid (){
    return ( $this->position < $this->pointer );
  }
  // }}}

  // public offsetExists(offset) {{{ 
  /**
   * offsetExists
   * 
   * @param mixed $offset 
   * @access public
   * @return bool
   */
  public function offsetExists ($offset){
    return ( false !== $this->search($offset));
  }
  // }}}

  // public offsetGet(offset) {{{ 
  /**
   * offsetGet
   * 
   * @param mixed $offset 
   * @access public
   * @throw Exception
   * @return mixed
   */
  public function offsetGet ($offset){
    $position = $this->search($offset);
    if ( false === $position ) return null;
    if ( $this->depth > 2){
      $this->iterator->setPosition($position);
      $return = $this->iterator;
    }
    else{
      $return = $this->get($position, 1);
    }
    return $return;
  }
  // }}}

  // public offsetSet(offset,value) {{{ 
  /**
   * offsetSet
   * 
   * @param mixed $offset 
   * @param mixed $value 
   * @access public
   * @throw Exception
   * @return void
   */
  public function offsetSet ( $offset , $value ){
    if (is_null($offset)){
      try{
        $position = $this->pointer++;
        if ( $position >= $this->size ){
          $new_size = $this->get_norm_size($position+1);
          $this->setSize($new_size);
        }
        $this->do_attach($position, $value[0], $value[1]);
        $this->_isSort = false;
        return;
      }
      catch(Exception $error){
        throw new BinaryException('Only arrays 0->key, 1->value can be added to the card.', 10002, $error);
      }
    }
    elseif(false === ($position = $this->search($offset))){
      $position = $this->pointer++;
      if ( $position >= $this->size ){
        $new_size = $this->get_norm_size($position+1);
        $this->setSize($new_size);
      }
      $this->do_attach($position, $offset, $value);
      $this->_isSort = false;
      return; 
    }
    else{
      $this->do_attach($position, $offset, $value);
      return;
    }
  }
  // }}}

  // public offsetUnset(offset) {{{ 
  /**
   * offsetUnset
   * 
   * @param mixed $offset 
   * @access public
   * @return void
   */
  public function offsetUnset ($offset){
    $position = $this->search($offset);
    if (false === $position) return;
    for($index = 0; $index < $this->depth; $index++)
      $this->set($position, $index, null);
    $this->_isSort = false;
  }
  // }}}

  // public count() {{{ 
  /**
   * count
   * 
   * @access public
   * @return int
   */
  public function count (){
    return $this->pointer;
  }
  // }}}

  // public attach(key,value) {{{ 
  /**
   * attach
   * 
   * @param mixed $key 
   * @param mixed $value 
   * @access public
   * @return void
   */
  public function attach( $key, $value ){
    $position   = $this->pointer++;
    if ( $position >= $this->size ){
      $new_size = $this->get_norm_size($position+1);
      $this->setSize($new_size);
    }
    $this->do_attach($position,$key,$value);
    $this->_isSort = false;
  }
  // }}}

  // public serialize() {{{ 
  /**
   * serialize
   * 
   * @access public
   * @return string
   */
  public function serialize (){
    $data = $this->get_header();
    if (self::MOD_FIXED_ARRAY !== $this->mod)
      return $data . $this->array;
  }
  // }}}

  // public unserialize(serialized) {{{ 
  /**
  * unserialize
  * 
  * @param string $serialized 
  * @access public
  * @return void
  */
  public function unserialize ($ser){
    $len    = unpack('V', $ser);
    $len    = reset($len);
    $data   = unpack("VLen/a{$len}Data",substr($ser, 0, $len+4));
    $data   = $data['Data'];
    $this->set_header( $data );
    if (self::MOD_FIXED_ARRAY !== $this->mod){
      $data = substr($ser,4+$len);
      $this->array->load($data);
    }
  }
  // }}}

}

/**
 * Class: ArrayMemory
 *
 * @package ArrayMap
 * @subpackage ArrayMemory
 *
 * @see Iterator
 * @see ArrayAccess
 * @see Countable
 * @see Serializable
 * 
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 */
class ArrayMemory implements Iterator , ArrayAccess, Countable, Serializable{

  /**
   * MEMORY_THREAD_WRAPPER 
   * 
   * @const string
   */
  const MEMORY_THREAD_WRAPPER = 'php://memory';
  
  /**
   * pointer 
   * 
   * @var int
   * @access protected
   */
  protected $pointer          = 0;

  /**
   * size 
   * 
   * @var int
   * @access protected
   */
  protected $size             = 0;

  /**
   * offset 
   * 
   * @var mixed
   * @access protected
   */
  protected $offset           = null;

  /**
   * position 
   * 
   * @var int
   * @access protected
   */
  protected $position         = 0;

  /**
   * shm 
   * 
   * @var int
   * @access protected
   */
  protected $shm              = 0;

  /**
   * depth 
   * 
   * @var int
   * @access protected
   */
  protected $depth            = 0;

  /**
   * offsets 
   * 
   * @var array
   * @access protected
   */
  protected $offsets          = array();

  /**
   * iterator 
   * 
   * @var mixed
   * @access protected
   */
  protected $iterator         = null;

  /**
   * gets
   *
   * @var array
   */
  protected $gets             = array();

  /**
   * sets
   *
   * @var array
   */
  protected $sets             = array();

  /**
   * string_format
   *
   * @var string
   */
  protected $string_format    = '';

  /**
   * count
   *
   * @var int
   */
  protected $count            = 0;

  // private getValue(position) {{{ 
  /**
   * getValue
   * 
   * @param mixed $position 
   * @access private
   * @return void
   */
  private function getValue($offset){
    if ($offset >= $this->count || $offset < 0) return null;
    $position     = $offset * $this->offset;
    $this->iterator->setPosition($position);
    return $this->iterator;
  }
  // }}}

  // private setValue(position,arrayvalue) {{{ 
  /**
   * setValue
   * 
   * @param mixed $offset 
   * @param array $value 
   * @access private
   * @return void
   * @deprecated
   */
  private function setValue($offset, array $value){
    if ($offset >= $this->count || $offset < 0) return null;
    $position               = $offset * $this->offset;
    fseek($this->shm, $position);
    for($iterator  = 0; $iterator < $this->depth; $iterator++){
      if (($format = $this->sets[$iterator]) instanceof BinaryFactory){
        $value[$iterator]   = $format->factory($value[$iterator])->toBinary();
      }
    }
    array_unshift($value, $this->string_format);
    $result = fwrite($this->shm, call_user_func_array('pack', $value), $this->offset);
    return $result;
  }
  // }}}

  // public init() {{{ 
  /**
   * init
   * 
   * @access public
   * @return void
   */
  public function init(){
    $args                 = func_get_args();
    $size                 = end($args);
    if (is_int($size)) {
      $this->size         = array_pop($args);
    }
    else $this->size      = 0;
    reset($args);
    if(empty($args))
      throw new BinaryException('Format not found.');
    $iterator             = 0;
    $this->gets           = array();
    $this->sets           = array();
    $this->offsets        = array();
    $this->offset         = 0;
    $ktemp                = 0;
    foreach( $args as $format){
      if ( $format instanceof BinaryFactory ){
        $this->gets[]     = $format;
        $this->sets[]     = $format;
        $this->offsets[]  = $ktemp = strlen($format->getTest()->toBinary());
        $this->offset     += $ktemp;
        ++$iterator;
      }
      else{
        $this->gets[]     = (0 < $iterator ? '/' : '') . "{$format}a" . ++$iterator;
        $this->sets[]     = $format;
        $this->offsets[]  = $ktemp = strlen(pack($format, null));
        $this->offset     += $ktemp;
      }
    }
    $this->string_format  = implode('', $this->sets);
    $this->depth          = $iterator;
    if (!$this->offset)
      throw new BinaryException('Cann\'t take a offset size');
    $this->position       = 0;
    $this->count          = 0;
    $this->shm            = fopen(self::MEMORY_THREAD_WRAPPER, 'w+b');
    if ( !is_resource($this->shm) ){
      throw new BinaryException('Cann\'t open thread wrapper.');
    }
    $this->iterator       = new MIterator( $this->shm, $this->offsets, $args);
  }
  // }}}

  // public __construct() {{{ 
  /**
   * __construct
   * 
   * @access public
   * @return void
   */
  public function __construct(){
    call_user_func_array( array($this, 'init'), func_get_args());
  }
  // }}}

  // public setSize(size) {{{ 
  /**
   * setSize
   * 
   * @param mixed $size 
   * @access public
   * @return void
   */
  public function setSize($size){
    if ($size < $this->size && $size < $this->count){
      ftruncate($this->shm, $size * $this->offset);
      $this->count = $size;
    }
    $this->size = $size;
  }
  // }}}

  // public getSize() {{{ 
  /**
   * getSize
   * 
   * @access public
   * @return int|null
   */
  public function getSize(){
    return $this->size;
  }
  // }}}

  // public getDepth() {{{ 
  /**
   * getDepth
   * 
   * @access public
   * @return void
   */
  public function getDepth(){
    return $this->depth;
  }
  // }}}

  // public load(data) {{{ 
  /**
   * load
   * 
   * @param mixed $data 
   * @access public
   * @return void
   */
  public function load(&$data){
    fseek($this->shm, 0);
    $length       = fwrite($this->shm, $data);
    $this->count  = intval(ceil($length/$this->offset));
    $this->rewind();
  }
  // }}}

  /**
   * fill
   *
   */
  public function fill( $start_position = 0, $end_position = 0){
    if ($start_position > $end_position || 
        $start_position >= $this->size || 
        $end_position >= $this->size ||
        $start_position < 0 ||
        $end_position < 0 ) return false;
    fseek($this->shm, $start_position * $this->offset);
    $data = pack("a{$this->offset}", null);
    for($iterator = $start_position; $iterator <= $end_position; $iterator++){
      fwrite($this->shm, $data, $this->offset);
    }
    $this->count = $end_position + 1;
    $this->rewind();
    return true;
  }
  
  /**
   * swap
   *
   * @param int $a
   * @param int $b
   * @return bool
   */
  public function swap($left, $right){
    if ($left >= $this->count || $right >= $this->count || $left < 0 || $right < 0) return false;
    $itemp    = $left * $this->offset;
    $jtemp    = $right * $this->offset;
            fseek( $this->shm, $itemp);
    $val1 = fread( $this->shm, $this->offset);
            fseek( $this->shm, $jtemp);
    $val2 = fread( $this->shm, $this->offset);
            fseek( $this->shm, - $this->offset, SEEK_CUR);
            fwrite($this->shm, $val1, $this->offset);
            fseek( $this->shm, $itemp);
            fwrite($this->shm, $val2, $this->offset);
    return true;
  }
  
  // public current() {{{ 
  /**
   * current
   * 
   * @access public
   * @return mixed
   */
  public function current (){
    return $this->getValue($this->position);
  }
  // }}}

  // public key() {{{ 
  /**
   * key
   * 
   * @access public
   * @return scalar
   */
  public function key (){
    return $this->position;
  }
  // }}}

  // public next() {{{ 
  /**
   * next
   * 
   * @access public
   * @return void
   */
  public function next (){
    ++$this->position;
  }
  // }}}

  // public rewind() {{{ 
  /**
   * rewind
   * 
   * @access public
   * @return void
   */
  public function rewind (){
    $this->position = 0;
    fseek($this->shm,0);
  }
  // }}}

  // public valid() {{{ 
  /**
   * valid
   * 
   * @access public
   * @return bool
   */
  public function valid (){
    return ($this->position < $this->count);
  }
  // }}}

  // public offsetExists(offset) {{{ 
  /**
   * offsetExists
   * 
   * @param mixed $offset 
   * @access public
   * @return bool
   */
  public function offsetExists ($offset){
    return ($offset < $this->count && $offset > 0);
  }
  // }}}

  // public offsetGet(offset) {{{ 
  /**
   * offsetGet
   * 
   * @param mixed $offset 
   * @access public
   * @throw BinaryException
   * @return mixed
   */
  public function offsetGet ($offset){
    return $this->getValue($offset);
  }
  // }}}

  // public offsetSet(offset,value) {{{ 
  /**
   * offsetSet
   * 
   * @param mixed $offset 
   * @param mixed $value 
   * @access public
   * @return void
   */
  public function offsetSet ( $offset , $value ){
    if (null === $offset || $offset === $this->count){
      $position     = $this->count++;
      return $this->setValue($position, (array)$value);
    }
    elseif($offset < $this->count && $offset >= 0){
      return $this->setValue($offset, (array)$value);
    }
    return null;
  }
  // }}}

  // public offsetUnset(offset) {{{ 
  /**
   * offsetUnset
   * 
   * @param mixed $offset 
   * @access public
   * @return void
   */
  public function offsetUnset ($offset){
    if ($offset >= $this->count || $offset < 0) return null;
    $position     = $offset * $this->offset;
    fseek($this->shm, $position);
    return fwrite($this->shm, pack("a{$this->offset}", null), $this->offset);
  }
  // }}}

  // public count() {{{ 
  /**
   * count
   * 
   * @access public
   * @return int
   */
  public function count (){
    return $this->count;
  }
  // }}}

  // public __toString() {{{ 
  /**
   * __toString
   * 
   * @access public
   * @return void
   */
  public function __toString(){
    $this->rewind();
    return fread($this->shm, $this->offset * $this->count);
  }
  // }}}

  // public serialize() {{{ 
  /**
   * serialize
   * 
   * @access public
   * @return string
   */
  public function serialize (){
    
  }
  // }}}

 // public unserialize(serialized) {{{ 
 /**
  * unserialize
  * 
  * @param string $serialized 
  * @access public
  * @return void
  */
 public function unserialize ($serialized){

 }
 // }}}

}

/**
 * Class: Binary{
 *
 * @package ArrayMap
 * @subpackage Binary
 *
 * 
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 */
interface Binary{
  
  /**
   * TAG
   *
   * @const
   */
  const TAG             = '?';

  /**
   * toBinary
   * @return string
   */
  public function toBinary();

  /**
   * fromBinary
   *
   * @param string $data
   */
  public function fromBinary($data);

}

/**
 * Class: BinaryException
 *
 * @package ArrayMap
 * @subpackage Binary
 *
 * 
 * @see Exception
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 */
class BinaryException extends Exception{

}

/**
 * Class: BinaryFactory{
 *
 * @package ArrayMap
 * @subpackage Binary
 *
 * 
 * @abstract
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 */
abstract class BinaryFactory{

  /**
   * test
   *
   * @var Binary
   */
  private $test = null;

  /**
   * ERROR_LOAD_BINARY_OBJECT
   * @const
   */
  const ERROR_LOAD_BINARY_OBJECT = 10001;

  /**
   * getBinary
   *
   * @abstract
   * @return Binary
   */
  abstract protected function getBinary();

  /**
   * factory
   *
   * @param string|null $data
   * @return Binary
   * @throw Exception
   */
  final public function factory($data = null){
    try{
      if (($data instanceof Binary) && ($data instanceof $this->test)){
        return $data;
      }
      $obj = $this->getBinary();
      $obj->fromBinary($data);
    }
    catch( BinaryException $error){
      throw new BinaryException("Cann't load a Binary Object.\n<br/>", self::ERROR_LOAD_BINARY_OBJECT, $error);
    }
    return $obj;
  }

  /**
   * __construct
   *
   */
  public function __construct(){

    $this->test = $this->getBinary();

  }

  /**
   * getTest
   * @return Binary
   */
  public function getTest(){

    return $this->test;

  }

}

/**
 * Class: Comparable{
 *
 * @package ArrayMap
 * @subpackage Binary
 *
 * 
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 */
interface Comparable{

  /**
   * compareTo
   *
   * @param Comparable $obj
   */
  public function compareTo(Comparable $obj);

}

/**
 * Class: HashMap
 *
 * @package ArrayMap
 * @subpackage HashMap
 *
 * 
 * @see ArrayMap
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 */
class HashMap extends ArrayMap{

  /**
   * NUM_HASH_FUNCTIONS 
   * 
   * @const int
   */
  const NUM_HASH_FUNCTIONS = 4;
  
  /**
   * MOD_HASH 
   * 
   * @const int
   */
  const MOD_HASH          = 3;

  /**
   * HASH_PROSTOE 
   * 
   * @const int
   * @link http://en.wikipedia.org/wiki/Cuban_prime
   */
  const HASH_PROSTOE      = 60493;//769;//3469;//60493;//1037233-1048576;//50000-60493;//23000-28813;

  /**
   *
   * NODE_ALIVE
   *
   * @const int
   */
  const NODE_ALIVE        = 5;

  /**
   *
   * NODE_PASSE
   *
   * @const int
   */
  const NODE_PASSE        = 80;

  /**
   *
   * MAX_LOAD_FACTOR
   *
   * @const int
   */
  const MAX_LOAD_FACTOR   = 0.85; // it used when there is a collision and need to full rehashed 

  /**
   * primes
   *
   * @var array
   */
  protected $primes       = array(
    13,         109,          193,          433,          769,          1201,         1453,
    /*2029,*/   3469,         3889,         4801,         10093,        12289,        13873,
    18253,      /*20173,*/    21169,        /*22189,*/    28813,        /*37633,*/    43201,
    47629,      /*60493,*/    63949,        65713,        69313,        73009,        76801,
    /*84673,*/  106033,       108301,       /*112909,*/   115249,       129793,       139969,
    142573,     147853,       169933,       172801,       178609,       181549,       /*193549,*/
    209089,     221953,       238573,       245389,       259309,       270001,       273613,
    /*280909,*/ 284593,       299569,       307201,       326701,       342733,       355009,
    363313,     367501,       397489,       410701,       415153,       424129,       433201,
    534253,     544429,       549553,       565069,       596749,       618349,       640333,
    645889,     685453,       720301,       /*762049,*/   786433,       823729,       /*842701,*/
    /*940801,*/ 961069,       967873,       995329,       1009201,      /*1016173,*/  1037233,
    1094449,
  );

  /**
   * prime
   *
   * @var int
   */
  protected $prime            = 0;

  /**
   * hash_params
   *
   * @var array
   */
  protected $hash_params      = array();

  /**
   * added
   *
   * @var string
   */
  protected $added            = array();

  // protected get(key,id=0) {{{ 
  /**
   * get
   * 
   * @param int $key 
   * @param int $position 
   * @access protected
   * @return void
   */
  protected function get($key, $position = 0){
    return $this->array[$key][$position+1];
  }
  // }}}

  // protected set(key,id=0,value) {{{ 
  /**
   * set
   * 
   * @param mixed $key 
   * @param int $position 
   * @param mixed $value 
   * @access protected
   * @return void
   */
  protected function set($key, $position = 0, $value){
    $this->array[$key][$position+1] = $value;
  }
  // }}}

  /**
   * get_norm_size
   *
   * @param mixed $size
   */
  protected function get_norm_size($size = 0){
    $nsize = parent::get_norm_size($size/self::MAX_LOAD_FACTOR);
    $prime = null;
    foreach($this->primes as $item){
      if ($item > $size && $item < $nsize){
        $prime = $item;
      }
      elseif($item > $nsize && null === $prime ){
        $prime = $item;
        break;
      }
      elseif($item > $nsize) break;
    }
    $this->prime = $prime;
    return $nsize;
  }

  /**
   * hash_init
   *
   */
  protected function hash_init(){
    for($iterator = 0; $iterator < self::NUM_HASH_FUNCTIONS; $iterator++){
      while(!isset($this->hash_params[$iterator])){
        $aconf = mt_rand(1, $this->prime - 1);
        $bconf = mt_rand(0, $this->prime - 1);
        for($jterator = 0; $jterator < $iterator; $jterator++)
          if ($this->hash_params[$jterator]['a'] === $aconf || $this->hash_params[$jterator]['b'] === $bconf) break;
        if ($jterator >= ($iterator - 1)){
          $this->hash_params[$iterator] = array( 'a' => $aconf, 'b' => $bconf);
        }
      }
    }
  }
  
  // protected search(id) {{{ 
  /**
   * search
   * 
   * @param mixed $position 
   * @access protected
   * @return false|int
   */
  protected function search($find){
    return $this->hash_search($find);
  }
  // }}}
  
  /**
   * hash_function
   *
   * @param mixed $key
   * @param int $position
   * @return false|int
   */
  protected function hash_function($hash_value, $position = 0 ){
    if ($hash_value instanceof Binary){
      $hash_value = $hash_value->toBinary();
    }
    $hash_value = (string)$hash_value;
    $hash_value ^= ($hash_value >> 20) ^ ($hash_value >> 12);
    $hash_value ^= ($hash_value >> 7) ^ ($hash_value >> 4);
    $hash_value = ($this->hash_params[$position]['a']) * $hash_value;
    $hash_value += $this->hash_params[$position]['b'];
    $hash_value = abs($hash_value % $this->prime);
    $hash_value = $hash_value % $this->size;
    return $hash_value;
  }

  /**
   * hash_search
   *
   * @param mixed $key
   + @return false|int Return false if table will bi full rehashed or int seek of new or searched 
   */
  protected function hash_search($key){
    if ($this->size > 0){
      $iterator_offset                  = $this->array->offset;
      $iterator                         = $this->array->iterator;
      for($index = 0; $index < self::NUM_HASH_FUNCTIONS; $index++){
        $hash                           = $this->hash_function($key, $index);
        $iterator->position             = $iterator_offset * $hash;
        $iterator->offset               = 0;
        if(0 === $iterator->compareIndex($key)){
          return $hash;
        }
      }
    }
    return false;
  }

  /**
   * hash_search_empty_slot
   *
   * @param mixed $key
   */
  public function hash_search_empty_slot($key, $exclude = false){
    for($index = 0; $index < self::NUM_HASH_FUNCTIONS; $index++){
      $hash     = $this->hash_function($key, $index);
      if ($hash === $exclude) continue;
      $iterator = $this->array[$hash];
      if (false === $iterator) continue;
      $tag      = ~($iterator[0]);
      if ((self::NODE_ALIVE & $tag) && (self::NODE_PASSE & $tag)){
        
        return $index;
      }
    }
    return false;
  }

  /**
   * do_attach
   *
   * @param mixed $position
   * @param mixed $key
   * @param mixed $value
   */
  protected function do_attach( $position, $key, $value ){
    $value = (array)$value;
    array_unshift($value, self::NODE_ALIVE, $key);
    $this->array[$position] = $value;
  }

  /**
   * quick_sort
   *
   * @param int $low
   * @param int $high
   */
  protected function quick_sort( $low, $high ){
    $itemp = $low;                
    $jtemp = $high;
    $xtemp = $this->get(intval( ( $low + $high ) / 2 ));
    do {
      while($itemp < $this->size){
        $current_value  = $this->get($itemp);
        if(-1 !== $this->_compare($current_value, $xtemp)) break;
        ++$itemp;
      }
      while( 0 <= $jtemp){
        $current_value  = $this->get($jtemp);
        if(1 !== $this->_compare($current_value, $xtemp)) break;
        if (null === $this->get($jtemp)) $this->_p = $jtemp;
        --$jtemp;
      }
      if($itemp <= $jtemp){           
        if ($itemp < $jtemp){
          $this->array->swap($itemp, $jtemp);
        }
        $itemp++; $jtemp--;
      }
    } while($itemp <= $jtemp);
    if( $low < $jtemp ) $this->quick_sort( $low, $jtemp );
    if( $itemp < $high ) $this->quick_sort( $itemp, $high );
  }

  /**
   * hash_insert
   *
   * @param mixed $key
   * @param mixed $value
   * @param mixed $init
   */
  protected function hash_insert($key, $value, $exclude = false){
    if ($this->pointer >= $this->size) return false; // must full rehash
    if (false === $exclude){
      $this->added = array();
    }
    foreach($this->added as $added){
      if (0 === $this->_compare($key, $added)){
        $this->hash_rebuild();
        return $this->hash_insert($key,$value);
      }
    }
    $this->added[]  = $key;
    $position = 0;  
    if (false === ($position = $this->hash_search_empty_slot($key, $exclude))){
      $step       = mt_rand(1, self::NUM_HASH_FUNCTIONS-1);
      $hash       = $exclude;
      for($index = 1; $index <= self::NUM_HASH_FUNCTIONS; $index++){
        $temp     = $this->hash_function($key, ($step*$index)%self::NUM_HASH_FUNCTIONS);
        if ($temp != $hash){
          $hash   = $temp;
          break;
        }
      }
      $iterator   = $this->array[$hash];
      $old        = array();
      $tag        = $iterator[0];
      if (!(self::NODE_PASSE & ~$tag)) ++$this->pointer;
      $iterator->next(); 
      while($iterator->valid()){
        $old[]    = $iterator->current();
        $iterator->next();
      }
      $this->do_attach($hash, $key, $value);
      return $this->hash_insert(array_shift($old), $old, $hash);
    }
    else{
      $hash       = $this->hash_function($key, $position);
      $this->do_attach($hash, $key, $value);
      $this->pointer += 1;
      return true;
    }
  }

  /**
   * hash_next
   *
   * @param int $position
   * @return int
   */
  protected function hash_next($position = 0){
    while($position < $this->size && (self::NODE_ALIVE & ~($this->array[$position][0]))) $position++;
    return $position;
  }

  /**
   * hash_mark_passe
   *
   */
  protected function hash_mark_passe(){
    $position = -1;
    while($this->size > ($position = $this->hash_next(++$position))){
      $this->array[$position][0] = self::NODE_PASSE;
    }
  }

  /**
   * hash_rebuild
   *
   */
  protected function hash_rebuild(){
    $this->hash_init();
    $this->hash_mark_passe();
    $old_size       = $this->size;
    $load_factor    = $this->pointer / $this->size;
    if (self::MAX_LOAD_FACTOR < $load_factor){
      $size         = $this->get_norm_size($this->pointer / self::MAX_LOAD_FACTOR);
      $this->array->setSize($size);
      $this->init_array($this->size, $size - 1);
      $this->size   = $size;
    }
    $count = $this->pointer;
    $this->pointer  = 0;
    $match = 0;
    $position = -1;
    while($old_size > ++$position){
      $iterator     = $this->array[$position];
      if (self::NODE_PASSE & ~($iterator->current())){
        continue;
      }
      $iterator[0]  = null;
      $iterator->next(); 
      $passe        = array();
      while($iterator->valid()){
        $passe[]    = $iterator->current();
        $iterator->next();
      }
      ++$match;
      $this->hash_insert(array_shift($passe), $passe);
    }
  }

  /**
   * get_header
   * @return string
   */
  protected function get_header(){
    $data             = array(
      'pointer'       => $this->pointer,
      '_isSort'       => $this->_isSort,
      'size'          => $this->size,
      'args'          => $this->args,
      'hash_params'   => $this->hash_params,
      'prime'         => $this->prime,
    );
    $data             = serialize($data);
    return pack('Va*', strlen($data), $data);
  }

  /**
   * set_header
   *
   * @param string $ser
   */
  protected function set_header( &$ser ){
    $data               = unserialize($ser);
    $args               = $data['args'];
    $size               = end($args);
    if (is_int($size)){
      $args[key($args)] = intval(floor($data['size'] * self::MAX_LOAD_FACTOR));
    }
    else
      $args[]           = intval(floor($data['size'] * self::MAX_LOAD_FACTOR));
    reset($args);
    call_user_func_array(array($this,'init'), $args);
    $this->pointer      = $data['pointer'];
    $this->prime        = $data['prime'];
    $this->_isSort      = $data['_isSort'];
    $this->hash_params  = $data['hash_params'];
  }
  
  /**
   * init_array
   *
   */
  protected function init_array($start, $end){
    $this->array->fill($start, $end);
  }

  /**
   * sort
   *
   */
  public function sort(){
    $this->_p = $this->size;
    $return = $this->quick_sort( 0, $this->size - 1 );
    $this->_isSort = true;
    for($index = $this->_p; $index < $this->size; $index++){
      $this->array[$index][0] = null;
    }
    return $return;
  }

  /**
   * rehash
   * @return bool
   */
  public function rehash(){
    return $this->hash_rebuild();
  }

  // public init(size,depth) {{{ 
  /**
   * init
   * 
   * @param mixed $size 
   * @param mixed $depth 
   * @access public
   * @return void
   * @throw Exception
   */
  public function init($size = 0, $depth = 2){
    $args                 = func_get_args();
    $this->args           = $args;
    array_unshift($args, 'C');
    $this->mod            = self::MOD_HASH;
    $class                = new ReflectionClass('ArrayString');
    $this->array          = $class->newInstanceArgs($args);
    $this->depth          = $this->array->getDepth();
    $this->size           = $this->get_norm_size($this->array->getSize());
    $this->array->setSize($this->size);
    $this->position       = 0;
    $this->_isSort        = false;
    $this->pointer        = 0;
    $this->iterator       = new HIterator($this->array);
  }
  // }}}

  /**
   * __construct
   *
   */
  public function __construct(){
    call_user_func_array( array($this, 'init'), func_get_args());
    $this->init_array(0, $this->size - 1); // only for construct object
    $this->hash_init();
  }

  // public setSize(size) {{{ 
  /**
   * setSize
   * 
   * @param int $size 
   * @access public
   * @return bool
   */
  public function setSize( $size ){
    $size = $this->get_norm_size($size);
    if ($size === $this->size){
      return false;
    }
    $this->array->setSize($size);
    $this->init_array($this->size, $size - 1);
    $this->size = $size;
    $this->hash_rebuild(); //must rehash
    return;
  }
  // }}}

  /**
   * current
   *
   */
  public function current (){
    if ( $this->depth > 3){
      $this->iterator->setPosition($this->position);
      $return = $this->iterator;
    }
    else{
      $return = $this->get($this->position, 1);
    }
    return $return;
  }

  // public next() {{{ 
  /**
   * next
   * 
   * @access public
   * @return void
   */
  public function next (){
    $this->position = $this->hash_next($this->position + 1);
  }
  // }}}

  // public rewind() {{{ 
  /**
   * rewind
   * 
   * @access public
   * @return void
   */
  public function rewind (){
    $this->position = $this->hash_next(0);
  }
  // }}}

  // public valid() {{{ 
  /**
   * valid
   * 
   * @access public
   * @return bool
   */
  public function valid (){
    return ($this->position < $this->size);
  }
  // }}}

  /**
   * offsetExists
   *
   * @param mixed $offset
   */
  public function offsetExists ($offset){
    return (false !== $this->search($offset));
  }

  // public offsetSet(offset,value) {{{ 
  /**
   * offsetSet
   * 
   * @param mixed $offset 
   * @param mixed $value 
   * @access public
   * @throw Exception
   * @return void
   */
  public function offsetSet ($offset , $value){
    if (null === $offset){
      $this->hash_insert($value[0], $value[1]);
      return;
    }
    elseif(false === ($position = $this->search($offset))){
      $this->hash_insert($offset, $value);
      return;
    }
    else{
      $this->do_attach($position, $offset, $value);
      return;
    }
  }
  // }}}

  /**
   * offsetGet
   *
   * @param mixed $offset
   */
  public function offsetGet ($offset){
    $position = $this->search($offset);
    if ( false === $position ) return null;
    if ( $this->depth > 3){
      $this->iterator->setPosition($position);
      $return = $this->iterator;
    }
    else{
      $return = $this->get($position, 1);
    }
    return $return;
  }

  // public attach(key,value) {{{ 
  /**
   * attach
   * 
   * @param mixed $key 
   * @param mixed $value 
   * @access public
   * @return void
   */
  public function attach( $key, $value ){
    $this->hash_insert($key, $value);
  }
  // }}}

}

/**
 * Class: HIterator
 *
 * @package ArrayMap
 * @subpackage HashMap
 *
 * 
 * @see AMIterator
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 */
class HIterator extends AMIterator{


  // private get(key,id=0) {{{ 
  /**
   * get
   * 
   * @param int $key 
   * @param int $position 
   * @access private
   * @return void
   */
  private function get($key, $position = 0){
    return $this->array[$key][$position];
  }
  // }}}

  // private set(key,id=0,value) {{{ 
  /**
   * set
   * 
   * @param mixed $key 
   * @param int $position 
   * @param mixed $value 
   * @access private
   * @return void
   */
  private function set($key, $position = 0, $value){
    $this->array[$key][$position] = $value;
  }
  // }}}

  // protected init(FArray array,depth) {{{ 
  /**
   * init
   * 
   * @param FArray $array 
   * @param int $depth 
   * @access protected
   * @return void
   */
  protected function init($array){
    $this->mod      = HashMap::MOD_HASH;
    $this->depth    = $array->getDepth();
    $this->array    = $array;
    $this->position = 0;
    $this->offset   = 1;
  }
  // }}}

  // public __construct(FArray array,depth) {{{ 
  /**
   * __construct
   * 
   * @param FArray $array 
   * @param int $depth 
   * @access public
   * @return void
   */
  public function __construct($array){
    $this->init($array);
  }
  // }}}

  // public setPosition(position) {{{ 
  /**
   * setPosition
   * 
   * @param int $position 
   * @access public
   * @return void
   */
  public function setPosition($position){
    $this->offset   = 1;
    $this->position = $position;
  }
  // }}}

  // public rewind() {{{ 
  /**
   * rewind
   * 
   * @access public
   * @return void
   */
  public function rewind (){
    $this->offset = 1;
  }
  // }}}

  /**
   * key
   *
   */
  public function key (){
    return $this->offset - 1;
  }

  // public count() {{{ 
  /**
   * count
   * 
   * @access public
   * @return int
   */
  public function count (){
    return $this->depth - 2;
  }
  // }}}

  /**
   * offsetExists
   *
   * @param mixed $offset
   */
  public function offsetExists ($offset){
    return (($offset+2) < $this->depth);
  }
  
  /**
   * offsetGet
   *
   * @param mixed $offset
   */
  public function offsetGet ($offset){
    $offset += 2;
    if ($offset >= $this->depth) return null;
    return $this->get($this->position, $offset);
  }
  
  /**
   * offsetSet
   *
   * @param mixed $value
   */
  public function offsetSet ( $offset , $value ){
    $offset += 2;
    if ($offset >= $this->depth) return null;
    $this->set($this->position, $offset, $value);
  }

  /**
   * offsetUnset
   *
   * @param mixed $offset
   */
  public function offsetUnset ( $offset ){
    $offset += 2;
    if ($offset >= $this->depth) return null;
    $this->set($this->position, $offset, null);
  }
}

/**
 * Class: MIterator
 *
 * @package ArrayMap
 * @subpackage ArrayMemory
 *
 * @see Iterator
 * @see ArrayAccess
 * @see Countable
 * 
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 */
class MIterator implements Iterator , ArrayAccess, Countable {

  /**
   * shm
   *
   * @var mixed
   */
  protected $shm = null;

  /**
   * offsets
   *
   * @var array
   */
  protected $offsets = array();

  /**
   * formats
   *
   * @var array
   */
  protected $formats = array();

  /**
   * position
   *
   * @var int
   */
  protected $position = 0;

  /**
   * pointer
   *
   * @var int
   */
  protected $pointer  = 0;

  /**
   * offset
   *
   * @var int
   */
  protected $offset   = 0;

  /**
   * depth
   *
   * @var int
   */
  protected $depth    = 0;

  /**
   * absoff
   *
   * @var array
   */
  protected $absoff   = array();

  private function sanitize_cell($offset_index, $value_cell){
    
  }
  
  /**
   * getValue
   *
   * @param mixed $offset
   * @return mixed
   */
  private function getValue($offset_index){
    if ($offset_index >= $this->depth || $offset_index < 0) return null;
    if ($this->pointer !== $offset_index){
      $this->pointer          = $offset_index;
      $position_offset        = $this->position;
      if (0 < $offset_index)
        $position_offset      += $this->absoff[$offset_index - 1];
      fseek($this->shm, $position_offset);
    }
    $value_cell               = fread($this->shm, $this->offsets[$offset_index]);
    $this->pointer += 1;
    $format_cell              = $this->formats[$offset_index];
    if ($format_cell instanceof BinaryFactory){
      return $format_cell->factory($value_cell);
    }
    
    $value_cell = unpack($format_cell,$value_cell);
    
    return reset($value_cell);
  }

  /**
   * setValue
   *
   * @param mixed $offset
   * @param mixed $value
   * @return bool
   */
  private function setValue($offset, $value){
    if ($offset >= $this->depth || $offset < 0) return null;
    if ($this->pointer !== $offset){
      $this->pointer  = $offset;
      $position       = $this->position;
      if (0 < $offset)
        $position     += $this->absoff[$offset-1];
      fseek($this->shm, $position);
    }
    ++$this->pointer;
    if (($format = $this->formats[$offset]) instanceof BinaryFactory){
      return fwrite($this->shm, $format->factory($value)->toBinary(), $this->offsets[$offset]);
    }
    return fwrite($this->shm, pack($format, $value), $this->offsets[$offset]);
  }

  /**
   * init
   *
   * @param mixed $shm
   * @param array $offsets
   * @param array $formats
   */
  protected function init( $shm, array $offsets, array $formats ){
    $this->shm      = $shm;
    $this->offsets  = $offsets;
    $iterator = 0;
    foreach($offsets as $off){
      $this->absoff[] = (0 === $iterator ? $off : $this->absoff[$iterator - 1] + $off);
      ++$iterator;
    }
    $this->formats  = $formats;
    $this->depth    = count($formats);
    $this->position = 0;
    $this->offset   = 0;
    $this->pointer  = 0;
  }

  /**
   * __construct
   *
   * @param mixed $shm
   * @param array $offsets
   * @param array $formats
   */
  public function __construct( $shm, array $offsets, array $formats ){
    $this->init($shm, $offsets, $formats);
  }

  // public setPosition(position) {{{ 
  /**
   * setPosition
   * 
   * @param int $position 
   * @access public
   * @return void
   */
  public function setPosition($position){
    $this->offset   = 0;
    $this->position = $position;
    $this->pointer  = 0;
    fseek($this->shm, $position);
  }
  // }}}
  
  // public current() {{{ 
  /**
   * current
   * 
   * @access public
   * @return mixed
   */
  public function current(){
    return $this->getValue($this->offset);
  }
  // }}}

  // public key() {{{ 
  /**
   * key
   * 
   * @access public
   * @return scalar
   */
  public function key (){
    return $this->offset;
  }
  // }}}

  // public next() {{{ 
  /**
   * next
   * 
   * @access public
   * @return void
   */
  public function next (){
    ++$this->offset;
  }
  // }}}

  // public rewind() {{{ 
  /**
   * rewind
   * 
   * @access public
   * @return void
   */
  public function rewind (){
    $this->offset   = 0;
    $this->pointer  = 0;
    fseek($this->shm, $this->position);
  }
  // }}}

  // public valid() {{{ 
  /**
   * valid
   * 
   * @access public
   * @return bool
   */
  public function valid (){
    return ($this->offset < $this->depth);
  }
  // }}}

  // public offsetExists(offset) {{{ 
  /**
   * offsetExists
   * 
   * @param mixed $offset 
   * @access public
   * @return bool
   */
  public function offsetExists ($offset){
    return ($offset < $this->depth);
  }
  // }}}

  // public offsetGet(offset) {{{ 
  /**
   * offsetGet
   * 
   * @param mixed $offset 
   * @access public
   * @throw Exception
   * @return mixed
   */
  public function offsetGet ($offset){
    return $this->getValue($offset);
  }
  // }}}

  // public offsetSet(offset,value) {{{ 
  /**
   * offsetSet
   * 
   * @param mixed $offset 
   * @param mixed $value 
   * @access public
   * @throw Exception
   * @return void
   */
  public function offsetSet ( $offset , $value ){
    if (!is_null($offset)){
      return $this->setValue($offset, $value);
    }
    else{
      $offset = $this->pointer;
      return $this->setValue($offset, $value);
    }
  }
  // }}}

  // public offsetUnset(offset) {{{ 
  /**
   * offsetUnset
   * 
   * @param mixed $offset 
   * @access public
   * @return void
   */
  public function offsetUnset ( $offset ){
    $this->setValue($offset, null);
  }
  // }}}

  // public count() {{{ 
  /**
   * count
   * 
   * @access public
   * @return int
   */
  public function count (){
    return $this->depth;
  }
  // }}}

}

/**
 * Class: ArrayString
 *
 * @package ArrayString
 * @subpackage HashMap
 *
 * @see Iterator
 * @see ArrayAccess
 * @see Countable
 * @see Serializable
 * 
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 */
class ArrayString implements Iterator , ArrayAccess, Countable, Serializable{

  /**
   * pointer 
   * 
   * @var int
   * @access protected
   */
  public $pointer          = 0;

  /**
   * size 
   * 
   * @var int
   * @access protected
   */
  public $size             = 0;

  /**
   * offset 
   * 
   * @var mixed
   * @access protected
   */
  public $offset           = null;

  /**
   * position 
   * 
   * @var int
   * @access protected
   */
  public $position         = 0;

  /**
   * shm 
   * 
   * @var int
   * @access protected
   */
  public $shm              = '';

  /**
   * depth 
   * 
   * @var int
   * @access protected
   */
  public $depth            = 0;

  /**
   * offsets 
   * 
   * @var array
   * @access protected
   */
  public $offsets          = array();

  /**
   * iterator 
   * 
   * @var mixed
   * @access protected
   */
  public $iterator         = null;

  /**
   * gets
   *
   * @var array
   */
  public $gets             = array();

  /**
   * sets
   *
   * @var array
   */
  public $sets             = array();

  /**
   * string_format
   *
   * @var string
   */
  public $string_format    = '';

  /**
   * count
   *
   * @var int
   */
  public $count            = 0;

  // private getValue(position) {{{ 
  /**
   * getValue
   * 
   * @param mixed $position 
   * @access private
   * @return void
   */
  private function getValue($offset){
    if ($offset >= $this->count || $offset < 0) return null;
    $position     = $offset * $this->offset;
    $this->iterator->setPosition($position);
    return $this->iterator;
  }
  // }}}

  // private setValue(position,arrayvalue) {{{ 
  /**
   * setValue
   * 
   * @param mixed $offset 
   * @param array $value 
   * @access private
   * @return void
   * @deprecated
   */
  private function setValue($offset, array $value){
    if ($offset >= $this->count || $offset < 0) return null;
    $position                         = $offset * $this->offset;
    for($iterator  = 0; $iterator < $this->depth; $iterator++){
      $format = $this->sets[$iterator];
      if ($format instanceof BinaryFactory){
        $value[$iterator]             = $format->factory($value[$iterator])->toBinary();
      }
    }
    array_unshift($value, $this->string_format);
    $replace_str                      = call_user_func_array('pack', $value);
    $offset                           = $position + $this->offset;
    for($index = $position, $jndex = 0; $index < $offset; $index++, $jndex++){
      $this->shm{$index}  = $replace_str{$jndex}; 
    }
    return $this->offset;
  }
  // }}}

  // public init() {{{ 
  /**
   * init
   * 
   * @access public
   * @return void
   */
  public function init(){
    $args                 = func_get_args();
    $size                 = end($args);
    if (is_int($size)) {
      $this->size         = array_pop($args);
    }
    else $this->size      = 0;
    reset($args);
    if(empty($args))
      throw new BinaryException('Format not found.');
    $iterator             = 0;
    $this->gets           = array();
    $this->sets           = array();
    $this->offsets        = array();
    $this->offset         = 0;
    $ktemp                = 0;
    foreach( $args as $format){
      if ( $format instanceof BinaryFactory ){
        $this->gets[]     = $format;
        $this->sets[]     = $format;
        $this->offsets[]  = $ktemp = strlen($format->getTest()->toBinary());
        $this->offset     += $ktemp;
        ++$iterator;
      }
      else{
        $this->gets[]     = (0 < $iterator ? '/' : '') . "{$format}a" . ++$iterator;
        $this->sets[]     = $format;
        $this->offsets[]  = $ktemp = strlen(pack($format, null));
        $this->offset     += $ktemp;
      }
    }
    $this->string_format  = implode('', $this->sets);
    $this->depth          = $iterator;
    if (!$this->offset)
      throw new BinaryException('Cann\'t take a offset size');
    $this->position       = 0;
    $this->count          = 0;
    $this->shm            = str_repeat(chr(0), $this->size * $this->offset);
    $this->iterator       = new SIterator( $this->shm, $this->offsets, $args);
  }
  // }}}

  // public __construct() {{{ 
  /**
   * __construct
   * 
   * @access public
   * @return void
   */
  public function __construct(){
    call_user_func_array( array($this, 'init'), func_get_args());
  }
  // }}}

  // public setSize(size) {{{ 
  /**
   * setSize
   * 
   * @param mixed $size 
   * @access public
   * @return void
   */
  public function setSize($size){
    if ($size < $this->size && $size < $this->count){
      $this->shm            = substr($this->shm, 0, $size * $this->offset);
      $this->count          = $size;
    }
    else{
      $this->shm            = substr_replace(str_repeat(chr(0), $size * $this->offset), $this->shm, 0, $this->size * $this->offset);
    }
    $this->count = $size;
    $this->iterator->shm = &$this->shm;
  }
  // }}}

  // public getSize() {{{ 
  /**
   * getSize
   * 
   * @access public
   * @return int|null
   */
  public function getSize(){
    return $this->size;
  }
  // }}}

  // public getDepth() {{{ 
  /**
   * getDepth
   * 
   * @access public
   * @return void
   */
  public function getDepth(){
    return $this->depth;
  }
  // }}}

  // public load(data) {{{ 
  /**
   * load
   * 
   * @param mixed $data 
   * @access public
   * @return void
   */
  public function load(&$data){
    $this->shm            = &$data;
    $this->rewind();
    $this->iterator->shm  = &$this->shm;
  }
  // }}}

  /**
   * fill
   *
   */
  public function fill( $start_position = 0, $end_position = 0){
    if ($start_position > $end_position || 
        $start_position >= $this->size || 
        $end_position >= $this->size ||
        $start_position < 0 ||
        $end_position < 0 ) return false;
    $start_pos            = $start_position * $this->offset;
    $end_pos              = ($end_position + 1) * $this->offset;
    for($index = $start_pos; $index < $end_pos; $index ++){
      $this->shm{$index}  = chr(0);
    }
    $this->count = $end_position + 1;
    $this->rewind();
    return true;
  }
  
  /**
   * swap
   *
   * @param int $a
   * @param int $b
   * @return bool
   */
  public function swap($left, $right){
    if ($left >= $this->count || $right >= $this->count || $left < 0 || $right < 0) return false;
    $itemp        = $left * $this->offset;
    $jtemp        = $right * $this->offset;
    for($index = 0; $index < $this->offset; $index++){
      $temp_byte  = $this->shm{$itemp + $index};
      $this->shm{$itemp + $index} = $this->shm{$jtemp + $index};
      $this->shm{$jtemp + $index} = $temp_byte;
    }
    return true;
  }
  
  // public current() {{{ 
  /**
   * current
   * 
   * @access public
   * @return mixed
   */
  public function current (){
    return $this->getValue($this->position);
  }
  // }}}

  // public key() {{{ 
  /**
   * key
   * 
   * @access public
   * @return scalar
   */
  public function key (){
    return $this->position;
  }
  // }}}

  // public next() {{{ 
  /**
   * next
   * 
   * @access public
   * @return void
   */
  public function next (){
    ++$this->position;
  }
  // }}}

  // public rewind() {{{ 
  /**
   * rewind
   * 
   * @access public
   * @return void
   */
  public function rewind (){
    $this->position = 0;
  }
  // }}}

  // public valid() {{{ 
  /**
   * valid
   * 
   * @access public
   * @return bool
   */
  public function valid (){
    return ($this->position < $this->count);
  }
  // }}}

  // public offsetExists(offset) {{{ 
  /**
   * offsetExists
   * 
   * @param mixed $offset 
   * @access public
   * @return bool
   */
  public function offsetExists ($offset){
    return ($offset < $this->count && $offset > 0);
  }
  // }}}

  // public offsetGet(offset) {{{ 
  /**
   * offsetGet
   * 
   * @param mixed $offset 
   * @access public
   * @throw BinaryException
   * @return mixed
   */
  public function offsetGet ($offset){
    return $this->getValue($offset);
  }
  // }}}

  // public offsetSet(offset,value) {{{ 
  /**
   * offsetSet
   * 
   * @param mixed $offset 
   * @param mixed $value 
   * @access public
   * @return void
   */
  public function offsetSet ( $offset , $value ){
    if (null === $offset || $offset === $this->count){
      $position     = $this->count++;
      return $this->setValue($position, (array)$value);
    }
    elseif($offset < $this->count && $offset >= 0){
      return $this->setValue($offset, (array)$value);
    }
    return null;
  }
  // }}}

  // public offsetUnset(offset) {{{ 
  /**
   * offsetUnset
   * 
   * @param mixed $offset 
   * @access public
   * @return void
   */
  public function offsetUnset ($offset){
    if ($offset >= $this->count || $offset < 0) return null;
    $position     = $offset * $this->offset;
    $offset       = $position + $this->offset;
    for($index = $position; $index < $offset; $index++){
      $this->shm{$index} = chr(0); 
    }
    return true;
  }
  // }}}

  // public count() {{{ 
  /**
   * count
   * 
   * @access public
   * @return int
   */
  public function count (){
    return $this->count;
  }
  // }}}

  // public __toString() {{{ 
  /**
   * __toString
   * 
   * @access public
   * @return void
   */
  public function __toString(){
    return $this->shm;
  }
  // }}}

  // public serialize() {{{ 
  /**
   * serialize
   * 
   * @access public
   * @return string
   */
  public function serialize (){
    
  }
  // }}}

 // public unserialize(serialized) {{{ 
 /**
  * unserialize
  * 
  * @param string $serialized 
  * @access public
  * @return void
  */
 public function unserialize ($serialized){

 }
 // }}}

}

/**
 * Class: SIterator
 *
 * @package StringArray
 * @subpackage HashMap
 *
 * @see Iterator
 * @see ArrayAccess
 * @see Countable
 * 
 * @author andrew scherbakov <kontakt.asch@gmail.com>
 * @version $id$
 * @copyright © 2013 andrew scherbakov
 */
class SIterator implements Iterator , ArrayAccess, Countable {

  /**
   * offsets
   *
   * @var array
   */
  public $offsets = array();

  /**
   * formats
   *
   * @var array
   */
  public $formats = array();

  /**
   * position
   *
   * @var int
   */
  public $position = 0;

  /**
   * pointer
   *
   * @var int
   */
  public $pointer  = 0;

  /**
   * offset
   *
   * @var int
   */
  public $offset   = 0;

  /**
   * depth
   *
   * @var int
   */
  public $depth    = 0;

  /**
   * absoff
   *
   * @var array
   */
  public $absoff   = array();

  /**
   * shm
   *
   * @var mixed
   */
  public $shm = '';

  /**
   * node_alive_bin
   *
   * @var string
   */
  public $node_alive_bin      = '';

  /**
   * getValue
   *
   * @param mixed $offset
   * @return mixed
   */
  private function getValue($offset_index){
    if ($offset_index >= $this->depth || $offset_index < 0) return null;
    $position_offset          = $this->position;
    if (0 < $offset_index)
      $position_offset        += $this->absoff[$offset_index - 1];
    $null_byte                = chr(0);
    $value_cell               = str_repeat($null_byte, $this->offsets[$offset_index]);
    $offset                   = $position_offset + $this->offsets[$offset_index];
    for($index = $position_offset, $jndex = 0; $index < $offset; $index++, $jndex++){
      $value_cell{$jndex}     = $this->shm{$index};
    }
    $format_cell              = $this->formats[$offset_index];
    if ($format_cell instanceof BinaryFactory){
      return $format_cell->factory($value_cell);
    }
    
    $value_cell = unpack($format_cell,$value_cell);
    
    return reset($value_cell);
  }

  /**
   * setValue
   *
   * @param mixed $offset
   * @param mixed $value
   * @return bool
   */
  private function setValue($offset, $value){
    if ($offset >= $this->depth || $offset < 0) return null;
    $position               = $this->position;
    if (0 < $offset)
      $position             += $this->absoff[$offset-1];
    $format                 = $this->formats[$offset];
    $replace_temp           = '';
    if ($format instanceof BinaryFactory){
      $replace_temp         = $format->factory($value)->toBinary();
    }
    else{
      $replace_temp         = pack($format, $value);
    }
    $offset                 = $position + $this->offsets[$offset];
    for($index = $position, $jndex = 0; $index < $offset; $index++, $jndex++){
      $this->shm{$index}    = $replace_temp{$jndex};
    }
  }

  /**
   * init
   *
   * @param mixed $shm
   * @param array $offsets
   * @param array $formats
   */
  protected function init( &$shm, array $offsets, array $formats ){
    $this->shm            = &$shm;
    $this->offsets        = $offsets;
    $iterator = 0;
    foreach($offsets as $off){
      $this->absoff[]     = (0 === $iterator ? $off : $this->absoff[$iterator - 1] + $off);
      ++$iterator;
    }
    $this->formats        = $formats;
    $this->depth          = count($formats);
    $this->position       = 0;
    $this->offset         = 0;
    $this->node_alive_bin = pack('C', HashMap::NODE_ALIVE);
  }

  /**
   * __construct
   *
   * @param mixed $shm
   * @param array $offsets
   * @param array $formats
   */
  public function __construct( &$shm, array $offsets, array $formats ){
    $this->init($shm, $offsets, $formats);
  }

  // public setPosition(position) {{{ 
  /**
   * setPosition
   * 
   * @param int $position 
   * @access public
   * @return void
   */
  public function setPosition($position){
    $this->offset   = 0;
    $this->position = $position;
  }
  // }}}

  /**
   * compareTo
   *
   * @param mixed $value_cell
   * @param mixed $offset_index
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   *
   * @return bool
   */
  public function compareTo($value_cell, $offset_index){
    if ($offset_index >= $this->depth || $offset_index < 0) return false;
    if($value_cell instanceof Binary){
      $value_cell             = $value_cell->toBinary();
    }
    else{
      $format_cell            = $this->formats[$offset_index];
      if ($format_cell instanceof BinaryFactory){
        $value_cell           = $format_cell->factory($value_cell)->toBinary();
      }
      else{
        $value_cell           = pack($format_cell, $value_cell);
      }
    }
    $position_offset          = $this->position;
    if (0 < $offset_index)
      $position_offset        += $this->absoff[$offset_index - 1];
    $offset                   = $position_offset + $this->offsets[$offset_index];
    for($index = $position_offset, $jndex = 0; $index < $offset; $index++, $jndex++){
      $byte1                  = $value_cell{$jndex};
      $byte2                  = $this->shm{$index};
      if($byte1 === $byte2){
        continue;
      }
      elseif($byte1 > $byte2){
        return -1;
      }
      elseif($byte1 < $byte2){
        return 1;
      }
    }
    return 0;
  }

  /**
   * compareIndex
   *
   * @param mixed $value_cell
   * @param mixed $offset_index
   * @todo do compare range
   *
   * @since 0.1 Start version
   * @author andrew scherbakov <kontakt.asch@gmail.com>
   * @copyright © 2013 andrew scherbakov
   *
   * @return bool
   */
  public function compareIndex($value_cell){
    if($value_cell instanceof Binary){
      $value_cell             = $value_cell->toBinary();
    }
    else{
      $format_cell            = $this->formats[1];
      if ($format_cell instanceof BinaryFactory){
        $value_cell           = $format_cell->factory($value_cell)->toBinary();
      }
      else{
        $value_cell           = pack($format_cell, $value_cell);
      }
    }
    $position_offset          = $this->position;
    if($this->node_alive_bin !== $this->shm{$position_offset}){
      return 1;
    }
    $offset                   = $this->absoff[1] - 1;
    for($index = $position_offset + 1, $jndex = 0; $jndex < $offset; $index++, $jndex++){
      $byte1                  = $value_cell{$jndex};
      $byte2                  = $this->shm{$index};
      if($byte1 === $byte2){
        continue;
      }
      elseif($byte1 > $byte2){
        return -1;
      }
      elseif($byte1 < $byte2){
        return 1;
      }
    }
    return 0;
  }
  
  // public current() {{{ 
  /**
   * current
   * 
   * @access public
   * @return mixed
   */
  public function current(){
    return $this->getValue($this->offset);
  }
  // }}}

  // public key() {{{ 
  /**
   * key
   * 
   * @access public
   * @return scalar
   */
  public function key (){
    return $this->offset;
  }
  // }}}

  // public next() {{{ 
  /**
   * next
   * 
   * @access public
   * @return void
   */
  public function next (){
    ++$this->offset;
  }
  // }}}

  // public rewind() {{{ 
  /**
   * rewind
   * 
   * @access public
   * @return void
   */
  public function rewind (){
    $this->offset   = 0;
  }
  // }}}

  // public valid() {{{ 
  /**
   * valid
   * 
   * @access public
   * @return bool
   */
  public function valid (){
    return ($this->offset < $this->depth);
  }
  // }}}

  // public offsetExists(offset) {{{ 
  /**
   * offsetExists
   * 
   * @param mixed $offset 
   * @access public
   * @return bool
   */
  public function offsetExists ($offset){
    return ($offset < $this->depth);
  }
  // }}}

  // public offsetGet(offset) {{{ 
  /**
   * offsetGet
   * 
   * @param mixed $offset 
   * @access public
   * @throw Exception
   * @return mixed
   */
  public function offsetGet ($offset){
    return $this->getValue($offset);
  }
  // }}}

  // public offsetSet(offset,value) {{{ 
  /**
   * offsetSet
   * 
   * @param mixed $offset 
   * @param mixed $value 
   * @access public
   * @throw Exception
   * @return void
   */
  public function offsetSet ( $offset , $value ){
    if (!is_null($offset)){
      return $this->setValue($offset, $value);
    }
  }
  // }}}

  // public offsetUnset(offset) {{{ 
  /**
   * offsetUnset
   * 
   * @param mixed $offset 
   * @access public
   * @return void
   */
  public function offsetUnset ( $offset ){
    $this->setValue($offset, null);
  }
  // }}}

  // public count() {{{ 
  /**
   * count
   * 
   * @access public
   * @return int
   */
  public function count (){
    return $this->depth;
  }
  // }}}

}

