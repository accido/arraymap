<?php defined('SYSPATH') or die('No direct script access.');
// Include array mapper

define("MAX_ARRAY_SIZE", 500000);

$m = memory_get_usage();
$obj = new ArrayMap( 'V', 'a20', MAX_ARRAY_SIZE);
for($i=0;$i<MAX_ARRAY_SIZE;$i++)
  $obj->attach( $i, sha1(rand()));
echo round((memory_get_usage()-$m)/1024/1024,2) . "\n<br/>\n";

/**
 * result ~ 11.45 MB 
 */


$m = memory_get_usage();
$obj = new ArrayMap( 'V', 'v', MAX_ARRAY_SIZE);
for($i=0;$i<MAX_ARRAY_SIZE;$i++)
  $obj->attach($i,rand());
echo round((memory_get_usage()-$m)/1024/1024,2) . "\n<br/>\n";

/**
 * result ~ 2.86 MB 
 */


$m = memory_get_usage();
$obj = new ArrayMap( 'V', 'V', MAX_ARRAY_SIZE);
for($i=0;$i<MAX_ARRAY_SIZE;$i++)
  $obj->attach($i,$i);
echo round((memory_get_usage()-$m)/1024/1024,2) . "\n<br/>\n";

/**
 * result ~ 3.82 MB ( ~4000000 byte) 
 */

$m = memory_get_usage();
$obj = new ArrayMap( 'V', 'V', MAX_ARRAY_SIZE);
for($i=0;$i<MAX_ARRAY_SIZE/2;$i++)//half size
  $obj->attach($i,$i);
echo round((memory_get_usage()-$m)/1024/1024,2) . "\n<br/>\n";

/**
 * result ~ 1.91 MB 
 */

$m = memory_get_usage();
$obj = new ArrayMap( MAX_ARRAY_SIZE, 2);
for($i=0;$i<MAX_ARRAY_SIZE;$i++)
  $obj->attach($i,$i);
echo round((memory_get_usage()-$m)/1024/1024,2) . "\n<br/>\n";

/**
 * result ~ 23.26 MB  
 */

$m = memory_get_usage();
$obj = new SplFixedArray(2);
$obj[0] = new SplFixedArray(MAX_ARRAY_SIZE);
$obj[1] = new SplFixedArray(MAX_ARRAY_SIZE);
for($i=0;$i<MAX_ARRAY_SIZE;$i++){
  $obj[0][$i] = $i;
  $obj[1][$i] = $i;
}
echo round((memory_get_usage()-$m)/1024/1024,2) . "\n<br/>\n";

/**
 * result ~ 19.07 MB 
 */

$m = memory_get_usage();
$obj = new SplFixedArray(MAX_ARRAY_SIZE);
for($i=0;$i<MAX_ARRAY_SIZE;$i++){
  $obj[$i] = new SplFixedArray(2);
  $obj[$i][0] = $i;
  $obj[$i][1] = $i;
}
echo round((memory_get_usage()-$m)/1024/1024,2) . "\n<br/>\n";

/**
 * result ~ 132.31 MB 
 */

$m = memory_get_usage();
$obj = array();
$obj[0] = range(1, MAX_ARRAY_SIZE);
$obj[1] = range(1, MAX_ARRAY_SIZE);
echo round((memory_get_usage()-$m)/1024/1024,2) . "\n<br/>\n";

/**
 * result ~ 80.29 MB 
 */

$m = memory_get_usage();
$obj = array();
for($i=0;$i<MAX_ARRAY_SIZE;$i++){
  $obj[$i][0] = $i;
  $obj[$i][1] = $i;
}
echo round((memory_get_usage()-$m)/1024/1024,2) . "\n<br/>\n";

/**
 * result ~ 143.15 MB 
 */


$m = memory_get_usage();
$obj = new ArrayMap( new IntFactory, 'V', MAX_ARRAY_SIZE);
for($i=MAX_ARRAY_SIZE;$i>0;$i--){
  $obj->attach(new IntBinary($i-1), MAX_ARRAY_SIZE-$i);
}
echo round( (memory_get_usage() - $m)/1024/1024,2) . "\n<br/>\n";
$obj->sort();
$data = $obj[new IntBinary(111111)];
echo round((memory_get_usage()-$m)/1024/1024,2) . "\n<br/>\n";
var_dump($data);

/**
 * result
 * 3.91 
 * 3.91 
 * int(388888)
 *
 */

$m    = memory_get_usage();
$arr    = new HashMap( 'V', 'V', ARRAY_SIZE );
for($i=ARRAY_SIZE;$i>=0;$i--){
  $arr[] = array( $i,ARRAY_SIZE - $i);
}
echo round( ( memory_get_usage() - $m) / 1024 / 1024, 2) . "\n<br/>\n";

/**
 * result
 * 9.16 MB (but real size hash - 786432 and 286432 are ready to write)
 *
 */
